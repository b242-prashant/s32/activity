let http = require("http");

http.createServer(function(requests,responses){
	if(requests.url == "/" && requests.method == "GET"){
		responses.writeHead(200, {'Content-Type':'text/plain'});
		responses.end('Welcome to the Booking System');
	}
	if(requests.url == "/profile" && requests.method == "GET"){
		responses.writeHead(200, {'Content-Type':'text/plain'});
		responses.end('Welcome to your profile');
	}
	if(requests.url == "/courses" && requests.method == "GET"){
		responses.writeHead(200, {'Content-Type':'text/plain'});
		responses.end("Here's our available courses");
	}
	if(requests.url == "/addCourse" && requests.method == "POST"){
		responses.writeHead(200, {'Content-Type':'text/plain'});
		responses.end("Add course to our resources");
	}
	

}).listen(4000);
console.log("Server is running successfully at localhost:4000");